#!/bin/bash

mkdir -p $1
mkdir -p $1/models
mkdir -p $1/data
mkdir -p $1/stories
mkdir -p $1/actions
mkdir -p $1/tests

chmod 777 $1
chmod 777 $1/models
chmod 777 $1/data
chmod 777 $1/stories
chmod 777 $1/actions
chmod 777 $1/tests

sudo docker run --privileged -d --name dind-test docker:dind

sudo docker run -v $1:/app -v $1/models:/app/models -v $1/data:/app/data -v $1/stories:/app/stories -v $1/tests:/app/tests -v $1/actions:/app/actions rasa/rasa:2.4.3-full init --no-prompt
